<?php

/**
 * Takes an ecrypted data ( encrypted with mcrypt with  MCRYPT_RIJNDAEL_128 and MCRYPT_MODE_CBC) and decrypts it.
 * @param $encData
 * @param bool $useMD5
 * @return null|string
 */

function __decrypt($encData, $useMD5 = false) {
    if (!$encData) return null;

    $key = NOTBENAMED;
    if ($useMD5) $key = substr(md5($key), 0, 16);

    $cipher = 'aes-128-cbc';  //Cipher method selcted.
    if (strlen($key) === 24) $cipher = 'aes-192-cbc';
    if (substr($encData,0,6)==='_2018_') {
        $cipher = 'aes-256-cbc-hmac-sha256';
        $encData = substr($encData, 6);
    }

    $allowedCipherMethods=openssl_get_cipher_methods();
    if (! in_array($cipher,$allowedCipherMethods ) ) {
        $errMsg = 'El m�todo de cifrado ' . $cipher . ' no es soportado en este servidor';
        LogMngr::log($errMsg, LogMngr::U_LOG_ERR);
        logArray($allowedCipherMethods, "metodosPermitidos", true);
        throw new MorlaException($errMsg);
    }

    if (!ctype_xdigit($encData)) return null;
    $enc = hex2bin($encData);
    $ivlen = openssl_cipher_iv_length($cipher);
    $iv = substr($enc, 0, $ivlen);
    $data = substr($enc, $ivlen);
    $newData=openssl_decrypt($data, $cipher, $key,OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);
    return $newData;
}

/**
 * Takes any data and encrypts it to be decrypted with mycrypt with MCRYPT_RIJNDAEL_128 and MCRYPT_MODE_ECB.
 * @param string $data
 * @param bool $useMD5
 * @return string
 */

function __encrypt(string $data, $useMD5 = false){

    while (strlen($data) % 16 != 0 ) $data .= "\0" ;

    $key = NOTBENAMED;
    if ($useMD5) $key = substr(md5($key), 0, 16);

    $cipher = "aes-128-cbc";    //Cipher method selcted.
    if (strlen($key) === 24) $cipher = 'aes-192-cbc';
    $ivlen = openssl_cipher_iv_length($cipher); // Defines the lenght of the 'iv' according to the cipher method.
    $iv = openssl_random_pseudo_bytes($ivlen);

    $allowedCipherMethods=openssl_get_cipher_methods();
    if (! in_array($cipher,$allowedCipherMethods ) ) {
        $errMsg = 'El m�todo de cifrado ' . $cipher . ' no es soportado en este servidor';
        LogMngr::log($errMsg, LogMngr::U_LOG_ERR);
        logArray($allowedCipherMethods, "metodosPermitidos", true);
        throw new MorlaException($errMsg);
    }

    $encryptedText = openssl_encrypt($data, $cipher, $key,OPENSSL_NO_PADDING | OPENSSL_RAW_DATA, $iv);
    $encryptedText = $iv.$encryptedText;
    return bin2hex($encryptedText);

}