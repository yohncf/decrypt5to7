### Decrypt 5 to 7

- Con php 7.2 desencriptar cadenas generadas en PHP 5.3

- Generar cadenas encriptadas en PHP 7.2 que puedan ser desencriptadas en PHP 5.3

    - Adicional: Modificar las funciones de php7 para que puedan trabajar con diferentes longitudes de llave (al menos 16 y 24).
    